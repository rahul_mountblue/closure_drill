function cacheFunction(cb) {
    // Should return a function that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.
    
    let cache = {};
    let testValue ; // this variable use in the process of testing

    return {
        invokeCb : function(...args) {
        let strKey = args.join(',');

        if(!cache[strKey]){
        testValue = 'Adding to cache!';
        // invoke callback function if it is not in cache
        console.log(testValue);
        cache[strKey] = cb(...args);
        let addValue = cache[strKey];
        return {addValue ,testValue};
        }
        // fetch result from cache 

        testValue = 'fetching from cache!';
        console.log(testValue);
        let fetchValue = cache[strKey];
        return {fetchValue,testValue};
        }
    }
    
}

module.exports = { cacheFunction  };
