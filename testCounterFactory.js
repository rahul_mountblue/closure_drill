const countFact = require('./counterFactory');

let countValue = countFact.counterFactory();
// testing
let testPass = true; 
if(countValue.increment() !== 1) {
    testPass = false;
}

if(countValue.increment() !== 2) {
    testPass = false;
}

if(countValue.increment() !== 3) {
    testPass = false;
}

if(countValue.decrement() !== 2) {
    testPass = false;
}

if(testPass) {
    console.log("test case pass for increment and decrement function");
}
