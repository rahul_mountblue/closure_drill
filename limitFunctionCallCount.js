function limitFunctionCallCount(cb, n) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
    
        let  invokeCount = 0;
        return {
            invokeCallback : function() {
                    
                while( invokeCount < n) {
                    invokeCount++;
                   return cb();
                }
                
                if( n-invokeCount <= 0) {
                    console.log(`Function can be called atmost ${n} times`);
                }
                return null;
            }
        }
}

module.exports = { limitFunctionCallCount };


