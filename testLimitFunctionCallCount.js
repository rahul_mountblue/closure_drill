const limitFun = require('./limitFunctionCallCount.js');

// callback function 
let callCount = 0;
function hello () {
    callCount++;
    console.log('hello')
}

let c = limitFun.limitFunctionCallCount( hello, 2);
 // testing for n = 2 callback function run only two times
 c.invokeCallback();
 c.invokeCallback();

 // In third call function should not be called

 c.invokeCallback();
 
 if( callCount > 2) {
     console.log("function must be called only 2 times. Something went wrong");
 }