const callCacheFunction = require('./cacheFunction');

// callback function 

function add(...args) {
    return args.reduce((s, e) => {
        return s += e;
      }, 0);
}

let bg = callCacheFunction.cacheFunction(add);

// testing 

const result1 = bg.invokeCb(10,20);
if(result1.testValue === 'Adding to cache!') {
    console.log(result1.addValue);
}

const result2 = bg.invokeCb(10,20);
if(result2.testValue === 'fetching from cache!') {
    console.log(result2.fetchValue);
}

